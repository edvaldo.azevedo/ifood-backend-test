package com.test.ifood.app.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neovisionaries.i18n.CountryCode;
import com.test.ifood.app.configurations.ApplicationProperties;
import com.test.ifood.app.entity.PlayList;
import com.test.ifood.app.service.TracksRecommendationService;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import com.wrapper.spotify.model_objects.specification.Recommendations;
import com.wrapper.spotify.model_objects.specification.TrackSimplified;
import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;

@Service
public class TracksRecommendationServiceImpl implements TracksRecommendationService {

	private static final int THIRTY_DEGREES_CELSIUS = 30;
	private static final int FIFTEEN_DEGREES_CELSIUS = 15;
	private static final int TEEN_DEGREES_CELSIUS = 10;
	private static final int FOURTEEN_DEGREES_CELSIUS = 14;

	private static final String PARTY = "party";
	private static final String POP = "pop";
	private static final String ROCK = "rock";
	private static final String CLASSICAL = "classical";
	
	@Autowired
	private ApplicationProperties properties;

	private SpotifyApi spotifyApi;
	private ClientCredentialsRequest clientCredentialsRequest;
	
	Logger logger = LoggerFactory.getLogger(TracksRecommendationServiceImpl.class);

	public List<PlayList> checkTempForRecommendationType(int temp) throws SpotifyWebApiException, IOException {

		List<PlayList> response;
		if (temp > THIRTY_DEGREES_CELSIUS) {
			logger.info("Temperatura acima de 30°c vamos curtir tracks party?");
			response = recommendationPlayListByGenres(PARTY);

		} else if (temp <= FIFTEEN_DEGREES_CELSIUS && temp <= THIRTY_DEGREES_CELSIUS) {
			logger.info("Temperatura entre de 15 e 30 °c um POP cairia bem agora!");

			response = recommendationPlayListByGenres(POP);

		} else if (temp <= TEEN_DEGREES_CELSIUS && temp <= FOURTEEN_DEGREES_CELSIUS) {
			logger.info("Temperatura entre de 10 e 14 °c os melhores do rock");

			response = recommendationPlayListByGenres(ROCK);
		} else {
			logger.info("Vamos relaxar um pouco e curtir os melhores clássicos");

			response = recommendationPlayListByGenres(CLASSICAL);
		}
		return response;
	}

	public List<PlayList> recommendationPlayListByGenres(String genero)
			throws SpotifyWebApiException, IOException {
		
		spotifyApi = new SpotifyApi.Builder().setClientId(properties.getSpotifyClientId())
					.setClientSecret(properties.getSpotifyClientSecretId()).build();
		
		clientCredentialsRequest = spotifyApi.clientCredentials().build();
		ClientCredentials clientCredentials = clientCredentialsRequest.execute();

		spotifyApi.setAccessToken(clientCredentials.getAccessToken());
		Recommendations recommendations = spotifyApi.getRecommendations().limit(properties.getSpotifyLimitPlayList()).market(CountryCode.BR)
				.seed_genres(genero).build().execute();
				
		return buildPlayListRecommendation(Arrays.asList(recommendations.getTracks()));



	}
	
	private List<PlayList> buildPlayListRecommendation(List<TrackSimplified> listTracks) {
		
		List<PlayList> playLists = new ArrayList<PlayList>();
		if (!listTracks.isEmpty()) {
			
			listTracks.forEach(track -> {
				PlayList playList = new PlayList();
				playList.setSongName(track.getName());
				Arrays.asList(track.getArtists()).forEach(artist ->playList.setArtistName(artist.getName()));
				playLists.add(playList);
			});
			
			
		}
		logger.debug("PlayList recomendada", playLists.toString());
		return playLists;
	}

	
}
