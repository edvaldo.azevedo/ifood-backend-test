package com.test.ifood.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.test.ifood.app.configurations.WebSecurityProperties;
import com.test.ifood.app.security.JWTLoginFilter;

@Configuration
@EnableWebSecurity
public class AppIfoodWebSecurity extends WebSecurityConfigurerAdapter  {
	
	
	@Autowired
	private WebSecurityProperties properties;
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeRequests()
			.anyRequest().permitAll()
			.and()
			
			.addFilterBefore(new JWTLoginFilter("/login", authenticationManager()),
	                UsernamePasswordAuthenticationFilter.class);
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser(properties.getUsername())
			.password(properties.getPassword())
			.roles(properties.getRole());
	}

}
