package com.test.ifood.app.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.ifood.app.entity.PlayList;
import com.test.ifood.app.exceptions.TracksRecommendationException;
import com.test.ifood.app.service.TracksRecommendationService;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;

@RestController
@RequestMapping("/app")
public class TracksRecommendationController {
	
	@Autowired
	private TracksRecommendationService tracksRecommendationService;
	
	
	@GetMapping(value="/tracks/{temp}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PlayList>> getPlayListRecommendationTemperature(@PathVariable int temp) throws SpotifyWebApiException, IOException, TracksRecommendationException{
		
		try {
			
			return new ResponseEntity<List<PlayList>>(tracksRecommendationService.checkTempForRecommendationType(temp),HttpStatus.OK);
		} catch (Exception e) {
			throw new TracksRecommendationException(e.getMessage());
		}
		
	}

}
