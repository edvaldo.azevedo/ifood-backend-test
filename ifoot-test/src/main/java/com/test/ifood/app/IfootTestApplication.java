package com.test.ifood.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IfootTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(IfootTestApplication.class, args);
	}

}
