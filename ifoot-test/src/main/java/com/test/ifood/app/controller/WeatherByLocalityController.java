package com.test.ifood.app.controller;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import com.test.ifood.app.entity.LocalityData;
import com.test.ifood.app.entity.Weather;
import com.test.ifood.app.exceptions.WeatherByLocalityException;
import com.test.ifood.app.service.WeatherByLocalityService;

@RestController
@RequestMapping("/app")
public class WeatherByLocalityController {
	
	@Autowired
	private WeatherByLocalityService weatherByLocalityService;
	
	@PostMapping(value = "/locality/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Weather> getWeatherByLocality( @RequestBody LocalityData locality) throws RestClientException, UnsupportedEncodingException, WeatherByLocalityException {
		
		try {
			Weather weather = new Weather();
			if (!locality.getCityName().isEmpty()) {
				weather = weatherByLocalityService.getWeatherByCityName(locality.getCityName());
				
			}else if (!locality.getLat().isNaN() && ! locality.getLon().isNaN()) {
				weather = weatherByLocalityService.getWeatherByLongAndLat(locality.getLon(), locality.getLat());
			}
			
			return new ResponseEntity<Weather>(weather,HttpStatus.OK);
		} catch (Exception e) {
			 throw new WeatherByLocalityException(e.getMessage());
		}
		
		
	}

}
