package com.test.ifood.app.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
@ConfigurationProperties
public class ApplicationProperties {

	private String spotifyUri;
	private String spotifyClientId;
	private String spotifyClientSecretId;
	private int spotifyLimitPlayList;
	private String weatherUri;
	private String weatherAppid;

	public String getSpotifyUri() {
		return spotifyUri;
	}

	public void setSpotifyUri(String spotifyUri) {
		this.spotifyUri = spotifyUri;
	}


	public String getWeatherUri() {
		return weatherUri;
	}

	public void setWeatherUri(String weatherUri) {
		this.weatherUri = weatherUri;
	}

	public String getWeatherAppid() {
		return weatherAppid;
	}

	public void setWeatherAppid(String weatherAppid) {
		this.weatherAppid = weatherAppid;
	}

	public String getSpotifyClientId() {
		return spotifyClientId;
	}

	public void setSpotifyClientId(String spotifyClientId) {
		this.spotifyClientId = spotifyClientId;
	}

	public String getSpotifyClientSecretId() {
		return spotifyClientSecretId;
	}

	public void setSpotifyClientSecretId(String spotifyClientSecretId) {
		this.spotifyClientSecretId = spotifyClientSecretId;
	}

	public int getSpotifyLimitPlayList() {
		return spotifyLimitPlayList;
	}

	public void setSpotifyLimitPlayList(int spotifyLimitPlayList) {
		this.spotifyLimitPlayList = spotifyLimitPlayList;
	}
	

}
