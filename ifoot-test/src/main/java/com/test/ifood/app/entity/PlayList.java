package com.test.ifood.app.entity;

import java.io.Serializable;

public class PlayList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String artistName;
	private String songName;

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

}
