package com.test.ifood.app.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.test.ifood.app.configurations.ApplicationProperties;
import com.test.ifood.app.entity.Weather;
import com.test.ifood.app.service.WeatherByLocalityService;

@Service
public class WeatherByLocalityServiceImpl implements WeatherByLocalityService {


	

	private final static String CITY_PARAM = "q";
	private final static String LONG_PARAM = "long";
	private final static String LAT_PARAM = "lat";
	private final static String UNIT_CELSIUS = "&units=metric";
	private final static String CHARACTER_EQUAL = "=";
	private final static String CHARACTER_INTERROGATION = "?";
	private final static String CHARACTER_AND = "&";
	private final static String APPID_PARAM = "appid";
	
	@Autowired
	private ApplicationProperties properties;

	private static String url;
	
	Logger logger = LoggerFactory.getLogger(WeatherByLocalityServiceImpl.class);

	

	@Override
	public Weather getWeatherByCityName(String cityName) throws RestClientException, UnsupportedEncodingException, JSONException {

		String result = getRestTemplate().getForObject(createAPIWeatherUrlByCityName(cityName), String.class);
		logger.debug("Response Weather", result);
		return buildWeatherObject(result);
	}

	@Override
	public Weather getWeatherByLongAndLat(Double lon, Double lat) throws JSONException {
		String result = getRestTemplate().getForObject(createAPIWeatherUrlByLongAndLat(lon,lat), String.class);
		
		logger.debug("Response Weather", result);
		return buildWeatherObject(result);

	}

	private String createAPIWeatherUrlByCityName(String cityName) throws UnsupportedEncodingException {

		if (!cityName.isEmpty()) {
			url = properties.getWeatherUri() + CHARACTER_INTERROGATION + CITY_PARAM + CHARACTER_EQUAL + URLDecoder.decode(cityName,"UTF-8")  + UNIT_CELSIUS
					+ CHARACTER_AND + APPID_PARAM + CHARACTER_EQUAL + properties.getWeatherAppid();
		}
		return url;

	}

	private String createAPIWeatherUrlByLongAndLat(Double lon, Double lat) {

		if (!lon.isNaN() && !lat.isNaN()) {
			url = properties.getWeatherUri() + CHARACTER_INTERROGATION + LAT_PARAM + CHARACTER_EQUAL + lat + CHARACTER_AND + LONG_PARAM
					+ CHARACTER_EQUAL + lon + UNIT_CELSIUS + CHARACTER_AND + APPID_PARAM + CHARACTER_EQUAL + properties.getWeatherAppid();
		}
		
		logger.debug("URL gerada :",url);
		return url;

	}
	
	private Weather buildWeatherObject(String object) throws JSONException {
		
		Weather weather = new Weather();
		JSONObject jsonObject = new JSONObject(object);
		if (object!=null) {
			weather.setCity(jsonObject.get("name").toString());
			weather.setCityId(jsonObject.get("id").toString());
			weather.setTempMin((jsonObject.getJSONObject("main").get("temp_min")).toString());
			weather.setTempMax((jsonObject.getJSONObject("main").get("temp_max")).toString());
			weather.setTemperature((jsonObject.getJSONObject("main").get("temp")).toString());
		}

		return weather;
	}

	
	public RestTemplate getRestTemplate() {
		return  new RestTemplate();
	}
}
