package com.test.ifood.app.service;

import java.io.IOException;
import java.util.List;

import com.test.ifood.app.entity.PlayList;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;

public interface TracksRecommendationService {
	
	
	List<PlayList> checkTempForRecommendationType(int temp) throws SpotifyWebApiException, IOException;

}
