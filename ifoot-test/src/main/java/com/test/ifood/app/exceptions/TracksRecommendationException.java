package com.test.ifood.app.exceptions;

public class TracksRecommendationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8712215667078643654L;

	private String msg;
	public TracksRecommendationException(String msg) {
		
		super(msg);
		this.msg = msg;
	}
}
