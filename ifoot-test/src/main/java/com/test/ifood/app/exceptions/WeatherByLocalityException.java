package com.test.ifood.app.exceptions;

public class WeatherByLocalityException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1041040519684667952L;

	private String msg;

	public WeatherByLocalityException(String msg) {
		super(msg);
		this.msg = msg;
	}

}
