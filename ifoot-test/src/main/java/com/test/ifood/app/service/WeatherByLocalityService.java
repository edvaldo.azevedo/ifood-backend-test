package com.test.ifood.app.service;

import java.io.UnsupportedEncodingException;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.web.client.RestClientException;

import com.test.ifood.app.entity.Weather;

public interface WeatherByLocalityService {

	Weather getWeatherByCityName(String cityName) throws RestClientException, UnsupportedEncodingException,JSONException;

	Weather getWeatherByLongAndLat(Double lon, Double lat) throws JSONException;

}
